

#'
#' # Start emacs as a daemon
#'
#' More in this [link](https://emacsredux.com/blog/2020/07/16/running-emacs-with-systemd/)
#' To increase speed in starting emacs see [this link](https://simpleit.rocks/linux/ubuntu/start-emacs-in-ubuntu-the-right-way/)
#'
#' First we need to turn Emacs into a `systemd service` that can be
#' started automatically during system starttup.
#'
#' From `emacs 26.1` you can automatically setup the daemon with
#'
#' > `systemctl --user enable --now emacs`
#'
#' This command copy a soft link in `/usr/lib/systemd/user/emacs.service`, pretty straightforward.
#'
#' 


#' # focus
#'
#' > `M-x package-install focus
#'

#' # multiple-cursors
#'
#' > `M-x install-package multiple-cursors
#'
#' in `.emacs` put:
#'
#' > `;; multiple-cursors`
#' > `(require 'multiple-cursors)`
#' > `(global-set-key (kbd "C-c m c") 'mc/edit-lines)`
#' > `(global-set-key (kbd "C-c m n") 'mc/mark-next-like-this)`
#' > `(global-set-key (kbd "C-c m f") 'mc/mark-previous-like-this)`
#' > `(global-set-key (kbd "C-c m a") 'mc/mark-all-like-this)`
#'
#' 


#' # python
#'
#' [installation easy](https://realpython.com/emacs-the-best-python-editor/)
#' [here more information](https://codeandoando.com/emacs-como-entorno-de-desarrollo-para-python/)
#' 
#' See [this web](http://tkf.github.io/emacs-jedi/latest/):
#' 
#' To install autocompletation
#'
#' 1. Install in the shell `sudo apt-get install virtualenv`
#' 1. Install in python `Jedi`
#' 1. Install in python `python-epc`
#' 1. Install in emacs `python-environment.el`
#' 1. Install in emacs `deferred.el`
#' 1. Install in emacs `auto-complete`
#' 1. Install in emacs `jedi`. Seems that `company-jedi` is more actual
#' 1. Install in emacs `jedi-direc`
#' 1. Remove `~/.emacs.d/.python-environments` and reinstall `emacs-jedi`
#' 1. In emacs `M-x jedi:install-server`

#' you can add to your .emacs:
#'
#' > ;; Standard el-get setup
#' > ;; (See also: https://github.com/dimitri/el-get#basic-setup)
#' > (add-to-list 'load-path "~/.emacs.d/el-get/el-get")
#' > 
#' > (unless (require 'el-get nil 'noerror)
#' >   (with-current-buffer
#' >       (url-retrieve-synchronously
#' >        "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
#' >     (goto-char (point-max))
#' >     (eval-print-last-sexp)))
#' > 
#' > (el-get 'sync)
#' > 
#' > 
#' > ;; Standard Jedi.el setting
#' > (add-hook 'python-mode-hook 'jedi:setup)
#' > (setq jedi:complete-on-dot t)
#' > 
#' > ;; Type:
#' > ;;     M-x el-get-install RET jedi RET
#' > ;;     M-x jedi:install-server RET
#' > ;; Then open Python file.
#'
#' # global gitignore
#'
#' To ignore files in git in the whole computer, from [here](https://simpleit.rocks/git/make-git-ignore-temporary-files-produced-by-emacs-and-vim-in):
#'
#' > git config --global core.excludesfile ~/.gitignore_global
#'
#' This will write in your `~/.gitconfig`:
#'
#' >  [core]
#' >      excludesfile = /home/user/.gitignore_global
#'
#' Then with `emacs /home/user/.gitignore_global`:
#' 
#' > # -*- mode: gitignore; -*-
#' > *~
#' > \#*\#
#' > /.emacs.desktop
#' > /.emacs.desktop.lock
#' > *.elc
#' > auto-save-list
#' > tramp
#' > .\#*
#' > 
#' > # Org-mode
#' > .org-id-locations
#' > *_archive
#' > 
#' > # flymake-mode
#' > *_flymake.*
#' > 
#' > # eshell files
#' > /eshell/history
#' > /eshell/lastdir
#' > 
#' > # elpa packages
#' > /elpa/
#' > 
#' > # reftex files
#' > *.rel
#' > 
#' > # AUCTeX auto folder
#' > /auto/
#' > 
#' > # cask packages
#' > .cask/
#' > dist/
#' > 
#' > # Flycheck
#' > flycheck_*.el
#' > 
#' > # server auth directory
#' > /server/
#' > 
#' > # projectiles files
#' > .projectile
#' > 
#' > # directory configuration
#' > .dir-locals.el
#'
#' 
