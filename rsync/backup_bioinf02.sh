#!/bin/bash

disks=( "/media/amartinezp/MyBook/datasets/" "/media/amartinezp/backup_UGCD/bioinf02/datasets" )

for disk in "${disks[@]}"
do
    echo "----------- START DATASETS --------------------------"
    echo "RSYNC in ${disk}:"
    if [ -d "${disk}" ]; then
	rsync -avzhP --exclude=".*" datasets@ir-bioinf02:/home/datasets/ "$disk"
	echo "---------------- DONE DATASETS ---------------------"
    fi
done


results=( "/media/amartinezp/MyBook/results/" "/media/amartinezp/backup_UGCD/bioinf02/results" )

for disk2 in "${results[@]}"
do
    echo "------------ START RESULTS -------------------------"
    echo "RSYNC in ${disk2}:"
    if [ -d "${disk2}" ]; then
	rsync -avzhP --exclude=".*" results@ir-bioinf02:/home/results/ "$disk2"
	echo "---------------- DONE RESULTS ---------------------"
    fi
done
