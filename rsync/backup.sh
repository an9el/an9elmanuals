#!/bin/bash

disks=( "/media/amartinezp/MyBook/datasets/" "/media/amartinezp/Seagate Backup Plus Drive/bioinf02/datasets" )

for disk in "${disks[@]}"
do
    echo "-------------------------------------"
    echo "RSYNC in ${disk}:"
    if [ -d "${disk}" ]; then
	rsync -avzhP --exclude=".*" datasets@bioinf01:/home/datasets/ "$disk"
	echo "---------------- DONE ---------------------"
    fi
done
