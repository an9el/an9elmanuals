
---
title: The use of ssh
author:
- name: Angel Martinez-Perez
date: "Started: 2021-07-03"
---

Angel Martinez-Perez [![](https://info.orcid.org/wp-content/uploads/2019/11/orcid_24x24.png)](https://orcid.org/0000-0002-5934-1454)

[[_TOC_]]
# :arrow_double_down: Install

Normally it will already be installed, but if not:

* install: `apt-get install openssh-server`
* start:   `systemctl enable ssh` to run it
* stop:    `sudo /etc/init.d/ssh stop`
* And install a client

* System-wide Configuration file: `emacs /etc/ssh/sshd_config`
* User's configuration file: `~/.ssh/config`.

Example of the user configuration file (`~/.ssh/config`) to add the key every time that reboot (see [this post](https://stackoverflow.com/questions/43382771/addkeystoagent-yes-ssh-config-not-working-on-mac#43393383))

> `Host gitlab`   
> `    HostName gitlab.com`   
> `    AddKeysToAgent yes`   
> `    IdentityFile ~/.ssh/id_ed25519`   

First you need to install the keychain (see [This web](https://www.cyberciti.biz/faq/ubuntu-debian-linux-server-install-keychain-apt-get-command/) )

## keychain instalation

> sudo apt-get install keychain

Then in the `~/.bashrc` file add:

> /usr/bin/keychain --clear $HOME/.ssh/id_ed25519
>
> source $HOME/.keychain/$HOSTNAME-sh

* Known host:    `~/.ssh/known_hosts`

## ssh key generation

* Generate key: `ssh-keygen -t ed25519 -C "keyName"` which
generates 2 keys:

> 1. `~/.ssh/id_ed25519` the private one, should not leave this machine   
> 2. `~/.ssh/id_ed25519.pub` the public one

* To automatize the login in the remote server:

`ssh-copy-id user@remoteIP:` carefull with the ":" final, it ask you for the password
of the remote server

Other form is to copy directly the public key into the rermote server `.ssh/authorized_keys`

* Modified the passphrase: `ssh-keygen -p -f ~/.ssh/id_ed25519`

## key deployment in gitlab

* Install in gitlab: `xclip -sel clip < ~/.ssh/id_ed25519.pub`
* sign in gitlab, click the avatar, settings, SSH keys, and paste the public key here
* Test if work `ssh -T git@gitlab.com`

# Normal uses

* connect: `ssh user@RemoteIP`
* Deploy the key in the remote server: `ssh-copy-id user@RemoteIP:`. With this no password will be asked
* chat client `ssh -t user@ip <command of chat>`, `-t` is a emulator of a terminal

# Advanced uses

## ssh as a proxy.



```{=html}
<div id="htmlwidget-4cc73527fd337b3c4efb" style="width:672px;height:200px;" class="DiagrammeR html-widget"></div>
<script type="application/json" data-for="htmlwidget-4cc73527fd337b3c4efb">{"x":{"diagram":"\ngraph LR\nA --> B\n"},"evals":[],"jsHooks":[]}</script>
```

```{=html}
<div id="htmlwidget-bccd2522ba4425aa8c5e" style="width:672px;height:200px;" class="DiagrammeR html-widget"></div>
<script type="application/json" data-for="htmlwidget-bccd2522ba4425aa8c5e">{"x":{"diagram":"\ngraph TB\nA(external1) --> B[firewall]\nB --> C(internal1)\nC --> E(internal2)\n"},"evals":[],"jsHooks":[]}</script>
```

```{=html}
<div id="htmlwidget-c96747a2333bc0568fce" style="width:672px;height:480px;" class="grViz html-widget"></div>
<script type="application/json" data-for="htmlwidget-c96747a2333bc0568fce">{"x":{"diagram":"digraph {\n\ngraph [layout = \"neato\",\n       outputorder = \"edgesfirst\",\n       bgcolor = \"white\"]\n\nnode [fontname = \"Helvetica\",\n      fontsize = \"10\",\n      shape = \"circle\",\n      fixedsize = \"true\",\n      width = \"0.5\",\n      style = \"filled\",\n      fillcolor = \"aliceblue\",\n      color = \"gray70\",\n      fontcolor = \"gray50\"]\n\nedge [fontname = \"Helvetica\",\n     fontsize = \"8\",\n     len = \"1.5\",\n     color = \"gray80\",\n     arrowsize = \"0.5\"]\n\n  \"1\" [fillcolor = \"#F0F8FF\", fontcolor = \"#000000\"] \n  \"2\" [fillcolor = \"#F0F8FF\", fontcolor = \"#000000\"] \n  \"1\"->\"2\" \n}","config":{"engine":"dot","options":null}},"evals":[],"jsHooks":[]}</script>
```


If for example, my work place have a proxy that block some web
pages that I want to access.

Or if I want to hide the web pages petitions from the proxy, I
can use the ssh as proxy between my browser and the work place rroxy:

> `ssh -D 9999 root@RemoteIP`

> `ssh -f -N -D 9999 root@RemoteIP -q`

Options:

**-f** to run it in the backgroun as a daemon   
**-N** do not execute any commands on localhost
**-q** quite mode, to avoid display messages like
`open failed: connect failed: connection timed out` in the terminal



This is a proxy socks in the server and I can configure my
browser to use this socks proxy to remove this block

I have to configure the browser or the internet connection
to use this `proxy SOCKS` with value `localhost:9999`. In firefox
`preferences -> network settings -> manual proxi configuration -> SOCKS Host = localhost, Port 9999`
No proxy for `localhost, 127.0.0.1` 

## ssh forwarding the X

This is to display a graphical program running in the remote
server in my server

> `ssh -X user@RemoteIP`

## To access a second server behind a firewall not visible.

**Situation**:

* **external1**: server outside the firewall   
* **internal1**: server inside the firewall visible from outside   
* **internal2**: server inside the firewall not visible from outside

I want to access **internal2** from **external1**

> `ssh -L 2020:<internal2IP>:22 root@internal1IP`

This create a 2020 port in the **external1** machine to access **internal2** machine

Then with the order:

> `ssh -p 2020 root@localhost`

in the **external1** machine I enter the **internal2** machine


### Other use of that is:

**Situation**:

* **external1** server outside a firewall with access to **internal1** server   
* **internal1** server inside a firewal with the `80` port not visible from **external1** server. For example, for security, is preferible not to spose a service running in this port to internet.

* You want to access a servicer running in the port `80` of the **internal1** server

You can use:

> `ssh -L 8080:localhost:80 root@internal1IP`

And I can access the port 80 with:

> `ssh -p 8080 root@localhost`

## reverse ssh tunneling

**Situation**:

* **external1**: a server outside a firewall without access to **internal1** server
* **internal1**: a server inside the firewall, and one person who has physical access to it.

I want to access from **external1** to **internal1** server.

From the **internal1** server I use this command:

> `ssh -R 2020:localhost:22 root@external1IP`   
> `ssh -R 2020:localhost:22 root@external1IP -o 'GatewayPorts yes'`

this create a 2020 port in the **external1** that points to the port 22 of **internal1**.

The `Gateway ports yes` allow hosts from the private network to connect back to your ssh client host.


I can login from the **external1** in **internal1** with the command:

> `ssh root@localhost -p2020`

# keep alive ssh tunnel

Use [autossh](http://www.debianadmin.com/autossh-automatically-restart-ssh-sessions-and-tunnels.html) for this:

> `autossh -M 20000 -f -N -R 2020:localhost:22 root@external1IP -C`

Option `-C` for compression the data before send it. Normally if you have good conection you don't have to use it
as it will reduce the speed of your connections.


# port management

## Test if a port is open:

> `nmap -p 999 localhost`

## Open/close a port

**ufw** to configure the rules of your firewall

> close: `sudo ufw deny 9999`   
> open: `sudo ufw allow 9999`

`sudo ufw status numbered` to see the active rules   
`sudo ufw delete number` to delete one rule

## to see your ip

> `curl ifconfig.me`

