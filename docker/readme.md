podman installation
================
Angel Martinez-Perez
2022-11-03

- <a href="#uninstall-docker" id="toc-uninstall-docker">Uninstall
  docker</a>
- <a href="#install-podman" id="toc-install-podman">install podman</a>
  - <a href="#configure-podman-registries"
    id="toc-configure-podman-registries">configure podman registries</a>
  - <a href="#pull-images" id="toc-pull-images">pull images</a>
- <a href="#volumes" id="toc-volumes">volumes</a>
- <a href="#ports" id="toc-ports">ports</a>

# Uninstall docker

<div>

> **Identify which docker packages have been installed**
>
> - `dpkg -l | grep -i docker`

</div>

<div>

> **Remove the packages**
>
> - `sudo apt-get purge -y docker-ce docker-ce-cli`

</div>

<div>

> **Remove also the docker related files**
>
> - sudo rm -rf /var/lib/docker /etc/docker
> - sudo rm /etc/apparmor.d/docker
> - sudo groupdel docker
> - sudo rm -rf /var/run/docker.sock

</div>

<div>

> **Deactivate network interface**
>
> - `ifconfig docker0 down`
> - `brctl delbr docker0`

</div>

# install podman

<div>

> **debian 11**
>
> - `sudo apt install podman`
> - `podman --version`
> - `podman info`

</div>

## configure podman registries

- `sudo vim /etc/containers/registries.conf`

and add the following line

`unqualified-search-registries = [ 'registry.access.redhat.com', 'registry.redhat.io', 'docker.io']`

## pull images

To pull or push images first we need to login the platform, for example
`docker.io`:

- `podman login docker.io`
- `podman search r-base`
- `podman pull docker.io/rocker/tidyverse:4.2.2`
- `podman logout  docker.io`

# volumes

This volume work because is interactive. We are able to write in the
volume.

- `podman run --name amartinezp --rm -it -v /home/amartinezp/kk:/home/angel:Z docker.io/rocker/tidyverse:4.2.2 R`

# ports

This volume don’t work don’t know why. It is the same as before but now
it launch `rstudio`:

- `podman run --name amartinezp --rm -it -p 8787:8787 -e PASSWORD="tit" -v /home/amartinezp/kk:/home/rstudio:Z docker.io/rocker/tidyverse:4.2.2`
