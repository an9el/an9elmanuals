---
title: "podman installation"
subtitle: "container examples"
title-block-banner: true
author: Angel Martinez-Perez
institute: IIB Sant Pau
date: last-modified
date-format: iso
format:
  gfm:
    toc: true
tbl-cap-location: bottom
execute:
  keep-md: true
  echo: false
  warning: false
---

# Uninstall docker

:::{.callout-note}
## Identify which docker packages have been installed
* `dpkg -l | grep -i docker`
:::

:::{.callout-note}
## Remove the packages
* `sudo apt-get purge -y docker-ce docker-ce-cli`
:::

:::{.callout-note}
## Remove also the docker related files
* sudo rm -rf /var/lib/docker /etc/docker
* sudo rm /etc/apparmor.d/docker
* sudo groupdel docker
* sudo rm -rf /var/run/docker.sock
:::

:::{.callout-note}
## Deactivate network interface

* `ifconfig docker0 down`
* `brctl delbr docker0`
:::


# install podman

:::{.callout-note}
## debian 11
```
sudo apt-get install \
  btrfs-progs \
  crun \
  git \
  golang-go \
  go-md2man \
  iptables \
  libassuan-dev \
  libbtrfs-dev \
  libc6-dev \
  libdevmapper-dev \
  libglib2.0-dev \
  libgpgme-dev \
  libgpg-error-dev \
  libprotobuf-dev \
  libprotobuf-c-dev \
  libseccomp-dev \
  libselinux1-dev \
  libsystemd-dev \
  pkg-config \
  uidmap \
  libapparmor-dev
```
  
* `sudo apt install podman`
* `podman --version`
* `podman info`
:::

## configure podman registries

* `sudo vim /etc/containers/registries.conf`

and add the following line

`unqualified-search-registries = [ 'registry.access.redhat.com', 'registry.redhat.io', 'docker.io']`

## pull images

To pull or push images first we need to login the platform, for example `docker.io`:

* `podman login docker.io`
* `podman search r-base`
* `podman pull docker.io/rocker/tidyverse:4.2.2`
* `podman logout  docker.io`

# volumes

This volume work because is interactive. We are able to write in the volume.

* `podman run --name amartinezp --rm -it -v /home/amartinezp/kk:/home/angel:Z docker.io/rocker/tidyverse:4.2.2 R`

# ports

This volume don't work don't know why. It is the same as before but now it launch `rstudio`:

* `podman run --name amartinezp --rm -it -p 8787:8787 -e PASSWORD="tit" -v /home/amartinezp/kk:/home/rstudio:Z docker.io/rocker/tidyverse:4.2.2`

:::{.callout-note}
## Comandos que no funcionan

* `podman run -u root --name rstudio -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -u root --name rstudio -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio:z -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -user-ns=keep-id -u root --name rstudio -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio:z -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio:z -e ROOT=true docker.io/rocker/tidyverse:4.2.2`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e ROOT=true docker.io/rocker/tidyverse:4.2.2` arranca pero no puede grabar `system('id') uid=1000(rstudio) gid=1000(rstudio) groups=1000(rstudio),27(sudo),50(staff)`
*
:::

:::{.callout-note}
## Comandos que SI funcionan
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`

:::

:::{.callout-note}
## Esto me lo hace bien pero me cambia los permisos
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e USER=amartinezp -e USERID=1001 -e GROUPID=1001 -e ROOT=TRUE docker.io/rocker/tidyverse:4.2.2`

:::

:::{.callout-note}
## With Sergui
* `podman run --name amartinezp -p 8787:8787 --rm -it docker.io/rocker/tidyverse:4.2.2 bash`


* `podman run --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/amartinezp -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`
* `podman run -user-ns=keep-id -u root --name amartinezp -p 8787:8787 --rm -it -v /home/amartinezp/kk:/home/rstudio -e ROOT=true docker.io/rocker/tidyverse:4.2.2 R`

:::

:::{.callout-note}
## Este funciona cuando el usuario es 1000

* `podman run --userns=keep-id -u root --name amartinezp -p 8787:8787 -e USERID=1000 -e GROUPID=1000 --rm -it -v /home/amartinezp/test:/home/rstudio docker.io/rocker/tidyverse:4.2.2`
* Incluso no hace falta poner las variables de entorno `USERID` ni `GROUPID`
:::


# docker rootless

[From here](https://docs.docker.com/engine/security/rootless/)
install all the necessary packages and then as non root run the following command:

```
curl -fsSL https://get.docker.com/rootless | sh
```

It will install the binaries in `~/bin` and you will be able to run docker as non root user
