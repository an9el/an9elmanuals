#!/usr/bin/env python3

## normal I/O streams are: STDIN, STDOUT, STDERR (standard input, output and error)

data = input("This will come from STDIN: ")
print("Now we write it to STDOUT: " + data)
print("Now we generate an error to STDERR: " + data + 1)
