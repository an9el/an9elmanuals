# The file_date function creates a new file in the current working directory, checks the date that the file was modified, and returns just the date portion of the timestamp in the format of yyyy-mm-dd. Fill in the gaps to create a file called "newfile.txt" and check the date that it was modified.


import os
import datetime

def file_date(filename):
    # Create the file in the current directory
    # filename = "newfile.txt"

    absolute_path = os.path.abspath(filename)

    with open(filename, "w"):
        pass
    timestamp = os.path.getmtime(filename)
    timestamp2 = datetime.date.fromtimestamp(timestamp)
    # Convert the timestamp into a readable format, then into a string
    print("{} is the last modified date".format(str(timestamp2)))
    return ("{}".format(str(timestamp2)[:10]))




