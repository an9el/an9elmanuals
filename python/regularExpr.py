## #! /usr/bin/env python

## better use "regex" which is compatible with the "re" module
## https://pypi.org/project/regex/
## https://docs.python.org/3/library/re.html

## Fill in the code to check if the text passed contains punctuation symbols: commas, periods, colons, semicolons, question marks, and exclamation points.
import re
def check_punctuation (text):
    result = re.search(r"[\,\.\:\;\?\!]", text)
    return result != None

print(check_punctuation("This is a sentence that ends with a period.")) # True
print(check_punctuation("This is a sentence fragment without a period")) # False
print(check_punctuation("Aren't regular expressions awesome?")) # True
print(check_punctuation("Wow! We're really picking up some steam now!")) # True
print(check_punctuation("End of the line")) # False


print(re.search(r"[^a-zA-Z ]","Wow! We're really picking up some steam now!."))
print(re.findall(r"cat|dog", "hola catalogo locodoglo"))


print(re.search(r"cat|dog", "hola catalogo"))


## Python regex to find multiple consecutive punctuations
The punctuation characters can be put into a character class is square brackets. Then it depends, whether the series of two or more punctuation characters consists of any punctuation character or whether the punctuation characters are the same.

In the first case curly braces can be appended to specify the number of minimum (2) and maximum repetitions. The latter is unbounded and left empty:

[...]{2,} # min. 2 or more

If only repetitions of the same character needs to be found, then the first matched punctuation character is put into a group. Then the same group (= same character) follows one or more:

([...])\1+

The back reference \1 means the first group in the expression. The groups, represented by the opening parentheses are numbered from left to right.

The next issue is escaping. There are escaping rules for Python strings and additional escaping is needed in the regular expression. The character class does not require much escaping, but the backslash must be doubled. Thus the following example quadruplicates the backslash, one doubling because of the string, the second because of the regular expression.

Raw strings r'...' are useful for patterns, but here both the single and double quotation marks are needed.

>>> import re
>>> test_cases = [
    "The quick '''brown fox",
    'The &&quick brown fox',
    'The quick\\brown fox',
    'The quick\\\\brown fox',
    'The -quick brown// fox',
    'The quick--brown fox',
    'The (quick brown) fox,,,',
    'The quick ++brown fox',
    'The "quick brown" fox',
    'The quick/brown fox',
    'The quick&brown fox',
    'The ""quick"" brown fox',
    'The quick,, brown fox',
    'The quick brown fox...',
    'The quick-brown fox',
    'The ((quick brown fox',
    'The quick brown)) fox',
    'The quick brown fox!!!',
    "The 'quick' brown fox" ]
>>> pattern_any_punctuation = re.compile('([-/\\\\()!"+,&\'.]{2,})')
>>> pattern_same_punctuation = re.compile('(([-/\\\\()!"+,&\'.])\\2+)')
>>> for t in test_cases:
    match = pattern_same_punctuation.search(t)
    if match:
        print("{:24} => {}".format(t, match.group(1)))
    else:
        print(t)

The quick '''brown fox   => '''
The &&quick brown fox    => &&
The quick\brown fox
The quick\\brown fox     => \\
The -quick brown// fox   => //
The quick--brown fox     => --
The (quick brown) fox,,, => ,,,
The quick ++brown fox    => ++
The "quick brown" fox
The quick/brown fox
The quick&brown fox
The ""quick"" brown fox  => ""
The quick,, brown fox    => ,,
The quick brown fox...   => ...
The quick-brown fox
The ((quick brown fox    => ((
The quick brown)) fox    => ))
The quick brown fox!!!   => !!!
The 'quick' brown fox



punctuation = re.compile('(([-/\\\\()!"+,&\'.])\\2+)')
x = 1
for t in test_cases:
    match = punctuation.search(t)
    if match:
        print "{0:2} {1:24} => {2}".format(x, t, match.group(1))
        x += 1

p = re.compile('[a-z]+')       
print(p.match('5 tempo'))
print(p.match('tempo'))

m = p.match('temo')
print(m.match('temporal'))

.-+

def check_web_address(text):
    pattern = r"^[a-zA-Z0-9_\.-\+]+\.[com|info|edu]$"
    result = re.search(pattern, text)
    return result != None

print(check_web_address("gmail.com"))
print(check_web_address("www@google.com"))

pattern = r'^([\w\.\-\\+]+)\.([com|info|edu])$'

print(re.search(pattern, text))

pattern = r'^[a-z]+\.(com|info|edu)$'

pattern = r'^[\+\.a-zA-Z0-9_-]+(\.[a-zA-Z]+)$'
text2 = 'gmail.com.inafo'


pattern = r'^([1-9]|10|11|12):[0-5][0-9] ?(AM|am|PM|pm)$'

pattern = r'\(([0-9]|[A-Z])[a-zA-Z0-9]*\)'


print(re.search(r" +[0-9]{5}(\D|(-[0-9]{5})?)", '90210 is a tv'))

## -------------------------------------------------------------------------
import re
checklist = ['1', '123', '123.', '123.4', '123.456', '.456', '123,', '123,4', '123,456', ',456', '0.,1', '0a,1', '0..1', '1.1.2', '100,000.99', '100.000,99', '0.1.', '0.abc']

pat = re.compile(r'^[0-9]*[.,]?[0-9]*$')

for c in checklist:
    if pat.match(c):
        print('{} : it matches'.format(c))
    else:
        print('{} : it does not match'.format(c))
        
## -------------------------------------------------------------------------
## capturing groups

import re

result = re.search(r"^(\w*), (\w*)$", "Lovelace, Ada")

print(result.group[0])
print(result.group[1])
print(result.groups())
"Name: {}, Surname: {}".format(result[2], result[1])

def rearrange_name(name):
    result = re.search(r"^(\w*[\w. ]]*), (\w*[\w. ]*)$", name)
    if result == None:
        return name
    return "{} {}".format(result[2], result[1])

print(rearrange_name("Kennedy, John F."))

## find all words with exact 5 letters, use \b for the start and the end
re.findall(r"\b[a-zA-Z]{5}\b", "a scary ghost appeared")
## print numbers or letter from 5 to 10 digits/letters
print(re.search(r"\w{5,10}", "a Scary8s ghosting5 appeared"))

## split sentences for any regular expresions
print(re.split(r"[.?!]", "One sentence. Another one? and the last one!"))
## capture also the symbol use to split
print(re.split(r"([.?!])", "One sentence. Another one? and the last one!"))

## substitute with regular expressions
print(re.sub(r"[\w.%+-]+@[\w.-]+", "[REDACTED]", "Received an email from user.45-84@gmail.com"))

## interchange the surname and name. We refer \2 as the second capturing group
print(re.sub(r"^([\w .]*), ([\w .]*)$", r"\2 \1", "Lovelace S., Ada"))





def transform_record(record):
    new_record = re.sub(r"([0-9-]{9,13})", r"+1-\1]", record)
    return new_record

print(transform_record("Sabrina Grenn,848-135-3456, System admin"))
print(transform_record("Anthony Basti,547-1353756, System develop"))

pattern = r"([0-9-]{9,13})"
print(re.search(pattern, "Sabrina Grenn,848-135-3456, System admin"))

