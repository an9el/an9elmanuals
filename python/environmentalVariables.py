#!/usr/bin/env python3

## in the shell (usually bash) we can type "env" to see the environmental variables.

## env
## echo $PATH

import os

print("HOME: " + os.environ.get("HOME", ""))
print("SHELL: " + os.environ.get("SHELL", ""))
print("SHELL: " + os.environ["SHELL"])
## the "get" method, return the second argument if the key is not present
## instead the "os.environ" return an error in this case
print("FRUIT: " + os.environ.get("FRUIT", "not present"))
## we can define the variable in shell by using:
## export FRUIT=Pinapple

#################################
## pass a modified environment variables to a subprocess
## more information about subprocess [here](https://docs.python.org/3/library/subprocess.html)
import subprocess

my_env = os.environ.copy()
my_env["PATH"] = os.pathsep.join(["/opt/myapp/", my_env["PATH"]])

result = subprocess.run(["myapp"], env = my_env)
## more parameters of the run option, for example:
## cwd = "folder", changes the working directory of the order
## timeout = seconds, stop the process the time exceeded the passed ones
## shell = True, this first execute the shell first and then run the process
