## operadores lógicos

pru = 2 + 2 == 4
print("correcto", pru)

print("No correcto", 2 + 2== 8)
print("correcto?", 2 + 2 != 8, "y esto", 2 + 2 < 8, "y esto?", 2 + 2 > 8)

ejem1 = "python"
ejem2 = "piton"

print("Son identicos?", ejem1 == ejem2)


# Operadores OR, AND y NOT:

print( "una opcion válida", (2 < 5) or (6 < 3))
print( "una opcion válida", (2 < 5) and (6 < 3))
print( "una opcion válida", not(2 < 5))

## expresiones de asignacion

contador = 1

contador += 2
print(contador)