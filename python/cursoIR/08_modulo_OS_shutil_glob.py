# El modulo os sirve para manejar el sistema operativo

import shutil
import csv
import os
import nmap

nmap.csv

os.ls

# directorio activo
print(os.getcwd())
# archivos de este directorio
print(os.listdir())

print(os.listdir("/home/"))

usuarios = os.listdir("/home")
print(usuarios[0])
# crear carpeta

os.mkdir("carpetita")
print(os.listdir())

os.rename("carpetita", "nuevo nombre")
os.rmdir("nuevo nombre")


# tamaño de archivos

print(os.stat("05_dic_tuplas.py"))

print(os.stat("05_dic_tuplas.py").st_size)

# os.remove() para borrar archivos
# os.isdecimal()


# 3
# crear un programa para borrar archivos que tengan el mismo tamaño


# primero los creo
rutas = ["origen/", "destino/"]
for i in rutas:
    os.mkdir(i)
    os.chdir(i)
    with open("kk.csv", mode="w", newline="") as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(i)
    print("El tamaño de archivo es: ", os.stat("kk.csv").st_size)
    os.chdir("../")


# luego miraría de borrar con os.stat(i).st_size


##############################################################################
# modulo shutil


origen = "ruta"
destino = "ruta2/carpetaNueva"
shutil.copy(origen + "hola.txt", destino + "hola_copia.txt")
# copiar todo (la carpeta nueva no existe)
shutil.copytree(origen, destino)

# comprimir una carpeta en zip con shutil
shutil.make_archivo("archivo_comprimido", "zip", "carpeta_a_comprimir")

for i in [1, 2, 3]:
    print(i)

MAX_PRIME = 100

sieve = [True] * MAX_PRIME
for i in range(2, MAX_PRIME):
    if sieve[i]:
        print(i)
        for j in range(i*i, MAX_PRIME, i):
            sieve[j] = False
