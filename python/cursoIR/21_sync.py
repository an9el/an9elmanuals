
import shutil
import os
import datetime

## hace una copia de seguridad de una carpeta y la mueve a otra carpeta

dt = datetime.datetime.now()
nombre = "Copia_seguridad_creada_dia{}_year_{}hora{}".format(dt.day, dt.year, dt.hour)
nomb2 = f"Copia {dt.day}, año {dt.year} y hora {dt.hour}"
print(nombre)
print(nomb2)


## comprimir el archivo.

shutil.make_archive(nombre, "zip", "carpeta_a_comprimir")

archivos = os.listdir()

for i in archivos:
    if i.startswith("Copia_seguridad_creada_dia") == True:
        shutil.move(i, "copias_seguridad")

