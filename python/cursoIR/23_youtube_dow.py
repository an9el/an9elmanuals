
from tkinter import *
from tkinter import ttk
from tkinter import Menu
from tkinter import messagebox
from pytube import YouTube

def popup():
    messagebox.showinfo("Info of the application", "YouTube downloader")

def precaucion():
    messagebox.showwarning("warning", "No valido para todos los videos")

def exit_app():
    if messagebox.askyesno("Exit", "Are you sure you want to exit?"):
        root.destroy()

def descargar():
    ## captura del enlace del video
    enlace = link.get()
    video = YouTube(enlace)
    des = video.streams.get_highest_resolution()
    des.download()
        
root = Tk()
root.title('YouTube downloader with python')

imagen = PhotoImage(file = "youtube.png")
foto = Label(root, image = imagen, bd = 0)
foto.grid(row = 0, column = 0)

## creamos el menu
menubar = Menu(root)
root.config(menu = menubar)

helpmenu = Menu(menubar, tearoff = 0)
menubar.add_cascade(label = "Info", menu = helpmenu)
helpmenu.add_command(label = "Use", command = popup)
helpmenu.add_command(label = "warning", command = precaucion)
helpmenu.add_command(label = "warning", command = precaucion)
helpmenu.add_command(label = "exit", command = exit_app)

instrucciones = Label(root, text = "put the link")
instrucciones.grid(row = 0, column = 1)

link = Entry(root)
link.grid(row = 1, column = 1)

boton = Button(root, text = "Download", command = descargar)
boton.grid(row = 2, column = 1)

root.mainloop()


