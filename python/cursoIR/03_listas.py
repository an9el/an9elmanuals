# Diferencias entre listas y variables:

var = 5
print(var)
lista = [4, 5, 6, "Hola que tal", "Esto es una lista"]
tupla = (4, 5, 6, "Hola que tal", "Esto es una tupla")
diccionario = {"mario": "25", "juan": "25", "pedro": "30"}

print(lista)

# Acceder a un indice

print("El primer elemento es:", lista[0], "y el tercero es:", lista[2],
      "y el último es: ", lista[-1], "y el penúltimo es:", lista[-2])

# imprimir desde un elemento

print(lista[2:])
print(lista[:3])


# Modificar listas

frutas = ["manzana", "pera", "kiwi"]
frutas.append("platano")
print(frutas)
frutas.append("platano")
print(frutas)

list = []

list.append(1)
list.append(4)

print(list)


# sublistas

list1 = [1, 2, 3]
list2 = [4, 5, 6]
list3 = [7, 8, 9]
listas = [list1, list2, list3]

print(listas)

# accedemos directamente a un elemento de la sublista
print(listas[1][1])
