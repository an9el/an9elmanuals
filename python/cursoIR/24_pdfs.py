## pip3 install PyMuPDF
import fitz ## es la librería PyMuPDF, tocatelos bien
import os
## https://pypi.org/project/PyMuPDF/

doc1 = "doc1.pdf"

doc_abierto = fitz.open(doc1)

## contar el numero de paginas
print("Number of pages de", doc1, "es ", doc_abierto.page_count)

doc_abierto.close()


