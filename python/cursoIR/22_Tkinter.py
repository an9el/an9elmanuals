https://tkdocs.com/tutorial/widgets.html


## sudo apt install python3-tk
## pip3 install tk-tools
## pip3 install pytube
  
import tk_tools

from tkinter import *


thinker.test()


## crear una ventana
ventana = Tk()

## titulo
ventana.title("Nombre de la aplicacion")

## etiquetas
texto1 = Label(ventana, text = "Primera etiqueta")
texto1.place(x = 40 y = 80)

## entrada de texto con posicionado absoluto
etiqueta1 = Label(ventana, text = "Recuadro texto con entry")
etiqueta1.place(x = 20, y = 30)


## funciones:

def enviar():
    print("has pulsado enviar")
    eti2 = Label(ventana, text = "has pulsado enviar")
    eti2.grid(row = 2, column = 1)


def botones1():
    eti3 = Label(ventana, text = "seleccion hecha")
    eti3.grid(row = 4, column = 1)




## recuadros
recuadro = Entry(ventana)
recuadro.place(x=10, y = 20)





## ###################################
from tkinter import *
ventana = Tk()
ventana.title("titulo")

## Con posicionado con grids

etiqueta1 = Label(ventana, text = "etiqueta1")
etiqueta1.grid(row = 0, column = 0)
recuadro = Entry(ventana)
recuadro.grid(row = 1, column = 0)
## recuadro.config(show = "*")

boton = Button(ventana, text = "enviar", command = enviar)
boton.grid(row = 2, column = 0)

rb1 = Radiobutton(ventana, text = "opc 1", command = botones1, value = 1)
rb1.grid(row = 3, column = 0)
rb2 = Radiobutton(ventana, text = "opc 2", command = botones1, value = 2)
rb2.grid(row = 3, column = 1)
rb3 = Radiobutton(ventana, text = "opc 3", command = botones1, value = 3)
rb3.grid(row = 3, column = 2)

### otra forma:


def sel():
    selection = "Seleccionaste la opción " + str(var.get())
    label.config(text = selection)

var = IntVar()

R1 = Radiobutton(ventana, text="Opción 1", variable=var, value=1, command=sel)
R1.pack(anchor=W)
R1.grid(row = 5, column = 0)

R2 = Radiobutton(ventana, text="Opción 2", variable=var, value=2, command=sel)
R2.pack(anchor=W)
R2.grid(row = 5, column = 1)

R3 = Radiobutton(ventana, text="Opción 3", variable=var, value=3, command=sel)
R3.pack(anchor=W)
R3.grid(row = 5, column = 2)

label = Label(ventana)
label.pack()


####################################################

## radiobutton de forma automatica

import tkinter as tk

root2 = tk.Tk()

v = tk.IntVar()
v.set(1)  # inicializar la elección

options = [("Opción 1", 1),
           ("Opción 2", 2),
           ("Opción 3", 3),
           ("Opción 4", 4)]

def show_choice():
    texto = "seleccionaste {}".format(v.get())
    label = Label(root2, text = texto)
    label.pack(anchor = "sw")


tk.Label(root2, 
         text="Elige una opción:",
         justify=tk.LEFT,
         padx=20).pack()

for option, value in options:
    tk.Radiobutton(root2, 
                   text=option,
                   padx=20, 
                   variable=v, 
                   command=show_choice,
                   value=value).pack(anchor=tk.W)


## cerrar
root2.mainloop()



####################################################
## un ejemplo de chatgpt


from tkinter import *

def show_selection():
    if var.get() == 1:
        label.config(text="Has seleccionado la opción 1", bg = "grey")
    elif var.get() == 2:
        label.config(text="Has seleccionado la opción 2", bg = "yellow")

root = Tk()

var = IntVar()

radio1 = Radiobutton(root, text="Opción 1", variable=var, value=1, command=show_selection)
radio1.pack(side = "top")

radio2 = Radiobutton(root, text="Opción 2", variable=var, value=2, command=show_selection)
radio2.pack(side = "top", anchor = "e")

label = Label(root)
label.pack(anchor = "sw")

root.mainloop()



## to make it executable with:
##   pyinstaller --onefile your_script.py










################################
## configuracion de los menus archivo, etc
## https://tkdocs.com/tutorial/menus.html


## Import the necessary modules:
import tkinter as tk
from tkinter import Menu

## Create a root window and set its title:
root = tk.Tk()
root.title('Menu Demo')

## Create a menu bar and assign it to the menu option of the root window:
menubar = Menu(root)
root.config(menu=menubar)

##Create a menu and add menu items to it:
file_menu = Menu(menubar)
file_menu.add_command(label='Exit', command=root.destroy)

## Add the menu to the menu bar:
menubar.add_cascade(label="File", menu=file_menu)


## Add submenus
sub_menu = Menu(file_menu)
sub_menu.add_command(label='Keyboard Shortcuts')
sub_menu.add_command(label='Color Themes')

file_menu.add_cascade(label="Preferences", menu=sub_menu)



## Run the application:
root.mainloop()



#########################
## el del curso


import tkinter as tk
from tkinter import Menu
from tkinter import messagebox

def Info_autor():
    messagebox.showinfo("Info autor", "Author Angel Martinez-Perez")
    
def CV_autor():
    messagebox.showwarning("CVs", "visit https://gitlab.com/an9el/")
    
def About_autor():
    messagebox.showerror("abouts", "Not created")


## Create a root window and set its title:
ventana = tk.Tk()
ventana.title("Menu curso")
## para evitar que el menu empiece con un separador
ventana.option_add('*tearOff', FALSE)

def exit_app():
    if messagebox.askyesno("Exit", "Are you sure you want to exit?"):
        ventana.destroy()

## creamos el menu
menues = Menu(ventana)
ventana.config(menu = menues)
menu1 = Menu(menues, tearoff = 0)

## añadimos elementos al menu
menu1.add_command(label = "Open")
menu1.add_command(label = "Save")
menu1.add_command(label = "Exit", command=exit_app)
## añadimos todas esas opciones al menu
menues.add_cascade(label = "File", menu = menu1)

menu2 = Menu(menues, tearoff = 1)

## añadimos elementos al menu
menu2.add_command(label = "Info", command = Info_autor)
menu2.add_command(label = "CV", command = CV_autor)
menu2.add_command(label = "About", command = About_autor)

## añadimos todas esas opciones al menu
menues.add_cascade(label = "About", menu = menu2)


## Listbox: The Listbox widget is used to display a list of items. The user can select one or more items from the list.
listbox = tk.Listbox(ventana)
listbox.insert(tk.END, "Item 1")
listbox.insert(tk.END, "Item 2")
listbox.pack()

## Scrollbar: The Scrollbar widget is used to add scrolling functionality to other widgets like Listbox or Text.
scrollbar = tk.Scrollbar(ventana)
listbox = tk.Listbox(ventana, yscrollcommand=scrollbar.set)
scrollbar.config(command=listbox.yview)
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
listbox.pack()

ventana.mainloop()



##########################################
## another example: calculadora de pies a metros:
from tkinter import *
from tkinter import ttk

def calculate(*args):
    try:
        value = float(feet.get())
        meters.set(int(0.3048 * value * 10000.0 + 0.5)/10000.0)
    except ValueError:
        pass

root = Tk()
root.title("Feet to Meters")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

feet = StringVar()
feet_entry = ttk.Entry(mainframe, width=7, textvariable=feet)
feet_entry.grid(column=2, row=1, sticky=(W, E))

meters = StringVar()
ttk.Label(mainframe, textvariable=meters).grid(column=2, row=2, sticky=(W, E))

ttk.Button(mainframe, text="Calculate", command=calculate).grid(column=3, row=3, sticky=W)

ttk.Label(mainframe, text="feet").grid(column=3, row=1, sticky=W)
ttk.Label(mainframe, text="is equivalent to").grid(column=1, row=2, sticky=E)
ttk.Label(mainframe, text="meters").grid(column=3, row=2, sticky=W)

for child in mainframe.winfo_children(): 
    child.grid_configure(padx=5, pady=5)

feet_entry.focus()
root.bind("<Return>", calculate)

root.mainloop()








###################################
## example of a scrollbar


from tkinter import *
from tkinter import ttk

root = Tk()
l = Listbox(root, height=5)
l.grid(column=0, row=0, sticky=(N,W,E,S))
s = ttk.Scrollbar(root, orient=VERTICAL, command=l.yview)
s.grid(column=1, row=0, sticky=(N,S))
l['yscrollcommand'] = s.set
ttk.Label(root, text="Status message here", anchor=(W)).grid(column=0, columnspan=2, row=1, sticky=(W,E))
root.grid_columnconfigure(0, weight=1)
root.grid_rowconfigure(0, weight=1)
for i in range(1,101):
    l.insert('end', 'Line %d of 100' % i)
root.mainloop()


###############################
## scale widget
s = ttk.Scale(parent, orient=HORIZONTAL, length=200, from_=1.0, to=100.0)



###############################
## Spinbox widgets are created using the ttk.Spinbox class:

spinval = StringVar()
s = ttk.Spinbox(parent, from_=1.0, to=100.0, textvariable=spinval)


###############################
## Progressbar widgets are created using the ttk.Progressbar class:

p = ttk.Progressbar(parent, orient=HORIZONTAL, length=200, mode='determinate')



from tkinter import *
root = Tk()
menu = Menu(root)
for i in ('One', 'Two', 'Three'):
    menu.add_command(label=i)
if (root.tk.call('tk', 'windowingsystem')=='aqua'):
    root.bind('<2>', lambda e: menu.post(e.x_root, e.y_root))
    root.bind('<Control-1>', lambda e: menu.post(e.x_root, e.y_root))
else:
    root.bind('<3>', lambda e: menu.post(e.x_root, e.y_root))
root.mainloop()


## #####################################
## Organizing complex interfaces
### https://tkdocs.com/tutorial/complex.html

###################################################################
## ultima clase de tkinter


from tkinter import *
from tkinter import ttk
from tkinter import Menu
from tkinter import messagebox
from pytube import YouTube

## video = YouTube("https://www.youtube.com/watch?v=Xx2S-rBLcHQ")
## video = YouTube("https://www.youtube.com/watch?v=pol3zFFyPow")
## descarga = video.streams.get_highest_resolution()
## descarga.download()

def popup():
    messagebox.showinfo("Info of the application", "YouTube downloader")

def precaucion():
    messagebox.showwarning("warning", "No valido para todos los videos")

def exit_app():
    if messagebox.askyesno("Exit", "Are you sure you want to exit?"):
        root.destroy()

def descargar():
    ## captura del enlace del video
    enlace = link.get()
    video = YouTube(enlace)
    des = video.streams.get_highest_resolution()
    des.download()
        
root = Tk()
root.title('YouTube downloader with python')

imagen = PhotoImage(file = "youtube.png")
foto = Label(root, image = imagen, bd = 0)
foto.grid(row = 0, column = 0)

## creamos el menu
menubar = Menu(root)
root.config(menu = menubar)

helpmenu = Menu(menubar, tearoff = 0)
menubar.add_cascade(label = "Info", menu = helpmenu)
helpmenu.add_command(label = "Use", command = popup)
helpmenu.add_command(label = "warning", command = precaucion)
helpmenu.add_command(label = "warning", command = precaucion)
helpmenu.add_command(label = "exit", command = exit_app)

instrucciones = Label(root, text = "put the link")
instrucciones.grid(row = 0, column = 1)

link = Entry(root)
link.grid(row = 1, column = 1)

boton = Button(root, text = "Download", command = descargar)
boton.grid(row = 2, column = 1)

root.mainloop()


