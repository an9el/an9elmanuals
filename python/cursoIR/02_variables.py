# variables

variable = 5
print(variable)
comida = "naranja"
print(comida)

# concatenar texto y variable

print("Mi comida favorita es la", comida)

# una variable almacena de todo

var1 = 3
var2 = 6
media = (var1 + var2) / 2
print("La media es", media)

not1,not2,not3 = (3,7,5)

print("La 1 es:", not1, ", la 2 es:", not2,", la 3 es:", not3)

import sys
sys.path
