## tuplas es como una lista pero no se pueden modificar los datos

tupla = (200, "hola", [1,2,3], 40)
print("indice 2, el cero y el último de la tupla", tupla[2], tupla[0], tupla[-1])

## esto daría error
## tupla.append(30)

# empaquetar tuplas
empleado = ("Mario", 26, "Masculino")

nombre,edad,sexo = empleado

print(edad)
print(nombre)










## diccionarios

## guarda información que podría ser cambiante 

dic = {"Mario":"26", "Jaime":"21","Manolo":"30"}
print(dic)
print(dic["Mario"])

# Modificar registros en diccionarios

dic["Mario"] = "27"
print(dic["Mario"])

## Eliminar un nombre

del(dic["Manolo"])
print(dic)