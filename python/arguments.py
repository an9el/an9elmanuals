#!/usr/bin/env python3

import sys
print(sys.argv)

import subprocess
print("program started on {}: ".format(subprocess.run(["date"])))
subprocess.run(["sleep", "2"])
result = subprocess.run(["ls", "this_file_does_not_exists"])
print(result.returncode)
result = subprocess.run(["host", "8.8.8.8"], capture_output = True)
print(result.stdout)
print(result.stdout.decode().split())
## the 'b in the beginning means that is an array of bits (because python
## do not know which encoding is used. (decode use by default UTF-8) which is the most common

## to capture the standard error. An example:
result = subprocess.run(["rm", "File_that_not_exist"], capture_output = True)
print(result.returncode)
print(result.stdout.decode())
print(result.stderr.decode())


## we can check the behavior by executing "./arguments.py one two"
##
## The exit status of a function in Linux is 0 if the function/program success, other if fails
##
## To see the last "exit status" we can print it in the shell with "echo $?"

import os

filename = sys.argv[1]

if not os.path.exists(filename):
    with open(filename, "w") as f:
        f.write("New file created! \n Please delete it")
else:
    print("Error, the file {} already exists".format(filename))
    sys.exit(1)
        
