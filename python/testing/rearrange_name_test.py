#!/usr/bin/env python3

from rearrange import rearrange_name
import unittest

## To test our rearrange_name function, lets create a class which
## inherits "unittest.TestCase".

## Then to run the test we will put unittest.main(), to run it automatically
class TestRearrange(unittest.TestCase):
    def test_basic(self):
        testcase = "Lovelace, Ada"
        expected = "Ada Lovelace"
        self.assertEqual(rearrange_name(testcase), expected)

    def test_empty(self):
        testcase = ""
        expected = ""
        self.assertEqual(rearrange_name(testcase), expected)

    def test_double_name(self):
        testcase = "Hopper, Grace M."
        expected = "Grace M. Hopper"
        self.assertEqual(rearrange_name(testcase), expected)

    def test_one_name(self):
        testcase = "Voltaire"
        expected = "Voltaire"
        self.assertEqual(rearrange_name(testcase), expected)

unittest.main()


## documentation
## https://docs.python.org/3/library/unittest.html
## https://docs.python.org/3/library/unittest.html#basic-example
## https://www.coursera.org/learn/python-operating-system/supplement/3WDVq/unit-test-cheat-sheet
## https://docs.python.org/3/library/unittest.html#command-line-interface
## https://docs.python.org/3/library/unittest.html#unittest.TestCase.assertRaises
## https://docs.python.org/3/library/unittest.html#organizing-test-code

## 
## Method                      Checks that
## assertEqual(a, b)           a == b
## assertNotEqual(a, b)        a != b
## assertTrue(x)               bool(x) is True
## assertFalse(x)              bool(x) is False
## assertIs(a, b)              a is b
## assertIsNot(a, b)           a is not b
## assertIsNone(x)             x is None
## assertIsNotNone(x)          x is not None
## assertIn(a, b)              a in b
## assertNotIn(a, b)           a not in b
## assertIsInstance(a, b)      isinstance(a, b)
## assertNotIsInstance(a, b)   not isinstance(a, b)


### It is also possible to check the production of exceptions, warnings, and log messages using the following methods:
## 
## Method                                            Checks that
## assertRaises(exc, fun, *args, **kwds)             fun(*args, **kwds) raises exc
## assertRaisesRegex(exc, r, fun, *args, **kwds)     fun(*args, **kwds) raises exc and the message matches regex r
## assertWarns(warn, fun, *args, **kwds)             fun(*args, **kwds) raises warn
## assertWarnsRegex(warn, r, fun, *args, **kwds)     fun(*args, **kwds) raises warn and the message matches regex r
## assertLogs(logger, level)                         The with block logs on logger with minimum level
## assertNoLogs(logger, level)                       The with block does not log on logger with minimum level

