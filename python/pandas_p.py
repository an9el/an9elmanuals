import pandas as pd
import numpy as np
import os

## https://pandas.pydata.org/docs/user_guide/10min.html
s = pd.Series([1, 3, 5, np.nan, 6, 8])
dates = pd.date_range("20130101", periods=6)

dates = pd.date_range("20130101", periods = 6, freq = "M")

df = pd.DataFrame(np.random.randn(6, 4), index = dates, columns = list("ABCD"))

df2 = pd.DataFrame(
    {
        "A": 1.0,
        "date": pd.Timestamp("20130102"),
        "C": pd.Series(1, index=list(range(4)), dtype="float32"),
        "D": np.array([3] * 4, dtype="int32"),
        "E": pd.Categorical(["test", "train", "test", "train"]),
        "F": "foo",
    }
)

print(df2.dtypes)
print(df2.date)
print(df.head())
print(df2.tail(2))
print(df.index)
print(df2.columns)
print(df.to_numpy())
df2.to_numpy()
print(df2.describe())
print(df.describe())
df.sort_index(axis = 1, ascending = False)
df.sort_values(by = 'C')


df.loc['20130531':, ["A", "B"]]
df.iloc[3:5, 0:2]
df.iat[3:5, 0:2]

df.iat[1, 1]


df2 = df.copy()
df2[df2["E"].isin(["two", "four"])]


