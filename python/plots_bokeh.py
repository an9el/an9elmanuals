## https://builtin.com/data-science/report-ready-plots-python
## https://docs.bokeh.org/en/latest/docs/first_steps.html

from bokeh.plotting import figure, save, output_file
import numpy as np

x = np.arange(0, 10, 1)
y = np.arange(0, 5, 0.5)

p1 = figure(width=800,
            height=400,
            x_axis_label='Time Since Experiment Start (Minutes)',
            y_axis_label = 'Distance Driven by Test Car (Kilometers)')

p1.circle(x, y, legend='Honda', color = 'red')
output_file('./FirstPlot.html', title = 'First_Plot')
save(p1)
