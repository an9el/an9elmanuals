## working with csv files:
##
## NEED to read:
## https://docs.python.org/3/library/csv.html
## https://realpython.com/python-csv/
## https://pandas.pydata.org/docs/user_guide/10min.html

import csv
import os

filename = "csv_example.csv"

with open(filename, "a") as file:
    for i in range(4):
        file.write("{},{},{}{}".format(i, 'a'*(i+1), i+1 , "\n"))

re = open(filename)
for r in re:
    index, name, index1 = csv.reader(re)

re.close()

os.remove(filename)

## write a csv file
frutas = [["manzana", "verde"], ["naranja","naranja"], ["platano","amarillo"]]

with open(filename, "w") as frutas_file:
    writer = csv.writer(frutas_file, delimiter = ";")
    writer.writerows(frutas)

with open(filename, "r") as check:
    color = csv.reader(check, delimiter = ';')
    counter = 0
    for rows in color:
        counter = counter + 1
        print("Row {}, with content {}".format(counter,rows))

os.remove(filename)

#####################################
## quitz
## We're working with a list of flowers and some information about each one. The create_file function writes this information to a CSV file. The contents_of_file function reads this file into records and returns the information in a nicely formatted block. Fill in the gaps of the contents_of_file function to turn the data in the CSV file into a dictionary using DictReader.

import os
import csv

# Create a file with data in it
def create_file(filename):
    with open(filename, "w") as file:
        file.write("name,color,type\n")
        file.write("carnation,pink,annual\n")
        file.write("daffodil,yellow,perennial\n")
        file.write("iris,blue,perennial\n")
        file.write("poinsettia,red,perennial\n")
        file.write("sunflower,yellow,annual\n")
        

# Read the file contents and format the information about each row
def contents_of_file(filename):
    return_string = ""
    
    # Call the function to create the file 
    create_file(filename)
    
    # Open the file
    with open(filename) as csvfile:
        # Read the rows of the file into a dictionary
        reader = csv.DictReader(csvfile)
        # Process each item of the dictionary
        for row in reader:
            return_string += "a {} {} is {}\n".format(row["color"], row["name"], row["type"])
    return return_string

#Call the function
print(contents_of_file("flowers.csv"))


## OTRA FORMA DIFERENTE
# Read the file contents and format the information about each row
def contents_of_file(filename):
    return_string = ""
    
    # Call the function to create the file 
    create_file(filename)
    
    # Open the file
    with open(filename) as csvfile:
        # Read the rows of the file into a dictionary
        reader = csv.DictReader(csvfile)
        content = list(reader)
        # Process each item of the dictionary
        for row in reader:
            return_string += "a {} {} is {}\n".format(row["color"], row["name"], row["type"])
    return return_string

#Call the function
print(contents_of_file("flowers.csv"))


## OTRA FORMA DIFERENTE
# Read the file contents and format the information about each row
def contents_of_file(filename):
    return_string = ""
    
    # Call the function to create the file 
    create_file(filename)
    
    # Open the file
    with open(filename) as csvfile:
        # Read the rows of the file into a dictionary
        reader = csv.reader(csvfile)
        content = list(reader)
        # Process each item of the dictionary
        for row in reader:
            return_string += "a {} {} is {}\n".format(row["color"], row["name"], row["type"])
    return return_string

#Call the function
print(contents_of_file("flowers.csv"))




#######################################################3
## Using the CSV file of flowers again, fill in the gaps of the contents_of_file function to process the data without turning it into a dictionary. How do you skip over the header record with the field names?

# Read the file contents and format the information about each row
def contents_of_file(filename):
    return_string = ""

    # Call the function to create the file 
    create_file(filename)

    # Open the file
    with open(filename) as csvfile:
        # Read the rows of the file
        rows = csv.reader(csvfile)
        next(rows, None)
        # Process each row
        for row in rows:
            #___ = row
            # Format the return string for data rows only

            return_string += "a {} {} is {}\n".format(row[1],row[0],row[2])
    return return_string

#Call the function
print(contents_of_file("flowers.csv"))
    
  file.write("name,color,type\n")
        file.write("carnation,pink,annual\n")
        file.write("daffodil,yellow,perennial\n")
        file.write("iris,blue,perennial\n")
        file.write("poinsettia,red,perennial\n")
        file.write("sunflower,yellow,annual\n")

dataF = [
    { 'name' : 'carnation', 'color': 'pink', 'type':'anual'},
    { 'name' : 'daffodil', 'color': 'yellow', 'type':'perennial'}
]

##dataF = { 'name' : 'carnation', 'color': 'pink', 'type':'anual'}

with open(filename, 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames = ['name','color','type'])
    writer.writeheader()
    writer.writerows(dataF)

# Open the file

with open(filename) as csvfile:
    return_string = []
    # Read the rows of the file into a dictionary
    reader = csv.DictReader(csvfile)
    # Process each item of the dictionary
    for row in reader:
        return_string += "a {} {} is {}\n".format(row["color"], row["name"], row["type"])
        print("{}".format(return_string))


 
