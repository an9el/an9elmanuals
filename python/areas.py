# example of a python module
# load it with "import areas as mymodule"
# access the functions with "mymodule.cicle(4)"

import math

def triangle(base, altura):
    return(base * altura/2)

def square(base, lado):
    return(base * lado)

def circle(radio):
    return(math.pi * radio**2)


