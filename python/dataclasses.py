###########################################################
from dataclasses import dataclass
from typing import Any
@dataclass
class carta:
    numero: str
    palo: str
    repartir: str = 'no'
    maximo: int = 12
    minimo: float = 1.0
    otros: Any = 42
    def quien_gana(self, other):
        """
        This function is a bad made function to know who win
        It can be made a lot better
        """
        if self.palo != other.palo:
            return 'gano yo'
        elif self.numero > other.numero:
            return 'gana EL'
        else:
            return 'gano YO'
    #: Aqui el metodo
    if __name__ == "__main__":
        f = quien_gana(carta('1','oros'), carta('1', 'bastos'))
        print(f)
        
            
        

###########################################################
carta1 = carta('12', 'oros')
Oron_dataclass = carta('1', 'oros')
Oron_tuple = ('1', 'oros')
Oron_tuple[0]
Oron_dict = {'numero': '1', 'palo': 'oros'}
Oron_dict['numero']
As_espadas = {'numero': '1', 'palo': 'espadas'}


carta1 == carta('12', 'oros')



## similar but worst the "namedtuple"
from collections import namedtuple

cartaNamed = namedtuple('cartaNamed', ['numero', 'palo'])
Oron_namedtuple = cartaNamed('1', 'oros')
Oron_namedtuple.palo
Oron_namedtuple == ('1','oros') ## !!!!


######################################
## pip install attrs
## attr no es estandar, añade más dependecias externas que dataclass
## Otras alternativas serían:
## * typing.NamedTuple
## * attrdict   ## https://pypi.org/project/attrdict/
## * plumber    ## https://pypi.org/project/plumber/
## * fields     ## https://pypi.org/project/fields/
import attr
from dataclasses import dataclass
@attr.s
class carta_attr:
    numero = attr.ib()
    palo = attr.ib()
    mano = attr.ib(factory = list)
    repartir = attr.ib(default = 'no')
    def maximo_num(self):
        return max(self.mano)
    def minimo_num(self):
        return min(self.mano)

copon = carta_attr('1','copas', [1,3,12,10,4])


