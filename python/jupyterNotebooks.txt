https://www.datacamp.com/community/tutorials/tutorial-jupyter-notebook
https://www.codecademy.com/articles/how-to-use-jupyter-notebooks
https://jupyter4edu.github.io/jupyter-edu-book/







#################3

Solve problems with Jupyter Notebooks

As of September 2020, your Jupyter Notebook lesson items have been migrated to Coursera Labs. Please refer to this article if you run into any issues accessing or using Coursera Labs.

Coursera Labs is a premium feature for in-browser assessments, designed to better support your learning journey. The feature is typically available to paid and financial aid learners. 

If you are auditing a course with Lab items behind the paywall after migration, we will not be able to support recovering access to your workspace. Check out the full announcement here.

----

Jupyter Notebooks are a third-party tool that some Coursera courses use for programming assignments. 

You can use this article to solve problems if your assessment is labelled “Notebook” in the course outline view. If your assessment launches into a Jupyter Notebook and is labeled “Ungraded Lab” or “Programming Assignment”, you can use the article “Solve Common Problems with Coursera Labs” to continue troubleshooting.

You can revert your code or get a fresh copy of your Jupyter Notebook mid-assignment. By default, Coursera persistently stores your work within each notebook.

To keep your old work and also get a fresh copy of the initial Jupyter Notebook, click File, then Make a copy.

We recommend keeping a naming convention such as “Assignment 1 - Initial” or “Copy” to keep your notebook environment organized. You can also download this file locally.
Refresh your notebook

There are two ways you can refresh your Jupyter Notebook, depending on whether or not your Jupyter Notebook assignment is running on our updated Labs framework. 

You can differentiate whether or not your Jupyter Notebook is running on our Labs framework, based on the assignment label in your course outline. A legacy Jupyter Notebook assignment will be called a “Notebook”. If your assignment is labeled “Ungraded Lab” or “Programming Assignment”, you can use the article “Solve Common Problems with Coursera Labs” for steps on how to refresh your Jupyter Notebook.

If you are using a legacy Jupyter Notebook (no ‘Lab Help’ option)

    Rename your existing Jupyter Notebook within the individual notebook view
    In the notebook view, add “?forceRefresh=true” to the end of your notebook URL
    Reload the screen
    You will be directed to your home Learner Workspace where you’ll see both old and new Notebook files.
    Your Notebook lesson item will now launch to the fresh notebook.

Find missing work

If your Jupyter Notebook files have disappeared, it means the course staff published a new version of a given notebook to fix problems or make improvements. Your work is still saved under the original name of the previous version of the notebook.

To recover your work:

    Find your current notebook version by checking the top of the notebook window for the title
    In your Notebook view, click the Coursera logo
    Find and click the name of your previous file

Unsaved work

Kernels are the execution engines behind the Jupyter Notebook UI. As kernels time out after 90 minutes of notebook activity, be sure to save your notebooks frequently to prevent losing any work. If the kernel times out before the save, you may lose the work in your current session.

How to tell if your kernel has timed out:

    Error messages such as Method Not Allowed appear in the toolbar area.
    The last save or auto-checkpoint time shown in the title of the notebook window has not updated recently
    Your cells are not running or computing when you “Shift + Enter”

To restart your kernel:

    Save your notebook locally to store your current progress
    In the notebook toolbar, click Kernel, then Restart
    Try testing your kernel by running a print statement in one of your notebook cells. If this is successful, you can continue to save and proceed with your work.
    If your notebook kernel is still timed out, try closing your browser and relaunching the notebook. When the notebook reopens, you will need to do Cell -> Run All or Cell -> Run All Above to regenerate the execution state.

Download and save Jupyter Notebooks 
You'll only be able to access your Jupyter Notebooks if you have a paid Coursera membership. If your subscription ends, you'll be put into audit mode and won't be able to access them. We recommend downloading all of your files before your subscription ends.

NOTE: If your Jupyter Notebook does not contain the ‘Lab Help’ modal, you can follow the instructions in this article for steps to download and save your Jupyter Notebook files on your computer. 

If you’d like to download your Jupyter Notebook  files locally:

    Click on the Jupyter logo, which will take you to the file tree view.
    Select the notebook that you want to download. Note that you’ll need to shut down the notebook if it is running.
    Click Download to download the file as a .ipynb file.

If you’d like to download the file in a different format (e.g. PDF, HTML), then you can click the Lab Help modal on the top right, and click “switch back to the old lab experience”. From here, you can click File, Download As, and then select your preferred file type.
