---
title: diagrams
subtitle: In quarto
author:
  - name: Jane Doe
format:
  html:
    embed-resources: true
    toc: true
    code-tools: true
      ##source: http://gitlab.com/an9el/an9elmanuals/quarto/diagrams.qmd
    code-copy: true
---

# mermaid


```{mermaid}
%%| label: fig-mermaid
%%| fig-width: 6
%%| fig-cap: |
%%|   How Quarto orchestrates rendering of documents: start with 
%%|   a qmd file, use the Knitr or Jupyter engine to perform the 
%%|   computations and convert it to an md file, then use Pandoc 
%%|   to convert to various file formats including HTML, PDF, 
%%|   and Word.
flowchart LR
  A[qmd] --> B(Knitr)
  A[qmd] --> C(Jupyter)
  B(Knitr) --> D[md]
  C(Jupyter) --> D[md]
  D[md] --> E(pandoc)
  E(pandoc) --> F(HTML)
  E(pandoc) --> G(PDF)
  E(pandoc) --> H(Word)
  E(pandoc) --> I{and more}
```



```{mermaid}
flowchart LR
  A[Hard edge] --> B(Round edge)
  B --> C{Decision}
  C --> D[Result one]
  C --> E((Result two))
```

## sequence diagrams

[here more](https://mermaid.js.org/syntax/sequenceDiagram.html)

```{mermaid}
sequenceDiagram
  participant Alice
  participant Bob
  Alice->>John: Hello John, how are you?
  loop Healthcheck
    John->>John: Fight against hypochondria
  end
  Note right of John: Rational thoughts <br/>prevail!
  John-->>Alice: Great!
  John->>Bob: How about you?
  Note over Alice,Bob: A typical interaction
  Bob-->>John: Jolly good!
```

## sizing

```{mermaid}
%%| theme: forest
%%| fig-width: 6.5
flowchart LR
  A[Hard edge] --> B(Round edge)
  B --> C{Decision}
```


## sequence

```{mermaid}
%%| label: fig-sequence
%%| fig-width: 6
sequenceDiagram
	actor Alice
	actor John
    Alice->>+John: Hello John, how are you?
    Alice->>+John: John, can you hear me?
    John-->>-Alice: Hi Alice, I can hear you!
    John-->>-Alice: I feel great!
```

## Flow

```{mermaid}
flowchart TD
  A[Christmas] -->|Get money| B(Go shopping)
  B --> C{Let me think}
  C -->|One| D[Laptop]
  C -->|Two| E[iPhone]
  C -->|Three| F[fa:fa-car Car]
```

```{mermaid}
graph TB
    sq[Square shape] --> ci((Circle shape))

    subgraph A
        od>Odd shape]-- Two line<br/>edge comment --> ro
        di{Diamond with <br/> line break} -.-> ro(Rounded<br>square<br>shape)
        di==>ro2(Rounded square shape)
    end

    %% Notice that no text in shape are added here instead that is appended further down
    e --> od3>Really long text with linebreak<br>in an Odd shape]

    %% Comments after double percent signs
    e((Inner / circle<br>and some odd <br>special characters)) --> f(,.?!+-*ز)

    cyr[Cyrillic]-->cyr2((Circle shape Начало));

     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:4px;
     class sq,e green
     class di orange
```


## Class

```{mermaid}
classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
```

## State

```{mermaid}
stateDiagram-v2
    [*] --> Still
    Still --> [*]
    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```

## ER

```{mermaid}
erDiagram
    CUSTOMER }|..|{ DELIVERY-ADDRESS : has
    CUSTOMER ||--o{ ORDER : places
    CUSTOMER ||--o{ INVOICE : "liable for"
    DELIVERY-ADDRESS ||--o{ ORDER : receives
    INVOICE ||--|{ ORDER : covers
    ORDER ||--|{ ORDER-ITEM : includes
    PRODUCT-CATEGORY ||--|{ PRODUCT : contains
    PRODUCT ||--o{ ORDER-ITEM : "ordered in"
```

## Gantt

```{mermaid}
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```

## User Journey


```{mermaid}
journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 3: Me
```

## Gitlab


```{mermaid}
gitGraph
    commit
    commit
    branch develop
    checkout develop
    commit
    commit
    checkout main
    merge develop
    commit
    commit
```


```{mermaid}
gitGraph:
    commit "Ashish"
    branch newbranch
    checkout newbranch
    commit id:"1111"
    commit tag:"test"
    checkout main
    commit type: HIGHLIGHT
    commit
    merge newbranch
    commit
    branch b2
    commit
```
	
	
## Pie


```{mermaid}
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```


## Mindmap


```{mermaid}
mindmap
  root((mindmap))
    Origins
      Long history
      Popularisation
        British popular psychology author Tony Buzan
    Research
      On effectivness<br/>and features
      On Automatic creation
        Uses
            Creative techniques
            Strategic planning
            Argument mapping
    Tools
      Pen and paper
      Mermaid
```




# dot

```{dot}
//| label: fig-dot
//| fig-width: 3
//| fig-cap: |
//|   How Quarto orchestrates rendering of documents: start with 
//|   a qmd file, use the Knitr or Jupyter engine to perform the 
//|   computations and convert it to an md file, then use Pandoc 
//|   to convert to various file formats including HTML, PDF, 
//|   and Word.
graph G {
  layout=neato
  qmd -- Knitr;
  qmd -- Jupyter;
  Knitr -- md;
  Jupyter -- md;
  md -- pandoc;
  pandoc -- HTML;
  pandoc -- PDF;
  pandoc -- Word;
  pandoc -- more;
}
```

```{dot}
graph G {
  layout=neato
  run -- intr;
  intr -- runbl;
  runbl -- run;
  run -- kernel;
  kernel -- zombie;
  kernel -- sleep;
  kernel -- runmem;
  sleep -- swap;
  swap -- runswap;
  runswap -- new;
  runswap -- runmem;
  new -- runmem;
  sleep -- runmem;
}
```
